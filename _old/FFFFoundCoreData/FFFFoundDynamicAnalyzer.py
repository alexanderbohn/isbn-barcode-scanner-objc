#
#  FFFFoundDynamicAnalyzer.py
#  FFFFoundCoreData
#
#  Created by fish on 4/7/09.
#  Copyright (c) 2009 Objects In Space And Time, LLC. All rights reserved.
#

##import objc;
from Cocoa import *
from Foundation import *

## why we are doing this in the first place
from BeautifulSoup import BeautifulSoup
import urllib2, re



class FFFFoundDynamicAnalyzer(NSObject):
	# these are declarations across the bridge
	# for the image grid
	recentUserNameDropdown = objc.IBOutlet()
	imageGridView = objc.IBOutlet()
	
	home_url = 'http://ffffound.com/home/'
	
	_data = objc.ivar()
    
	def init(self):
		self = super(FFFFoundDynamicAnalyzer, self).init()
		NSLog(u"FFFFoundDynamicAnalyzer python class initialized")
		
		
		if (self is None):
			return None
		self._data = []
		return self
	
	def awakeFromNib(self):
		NSLog(u"FFFFoundDynamicAnalyzer.awakeFromNib()")
		
		### [imageGridView setDelegate:self]
		### doesn't scope the same in python
		self.imageGridView.setDelegate_(self.imageGridView)
		self.imageGridView.setDataSource_(self.imageGridView)
		self.imageGridView.setDraggingDestinationDelegate_(self.imageGridView)
		
		NSLog(u"FFFFoundDynamicAnalyzer.awakeFromNib(): ATTEMPTING TO MAKE SOUP")
		
		url = 'http://ffffound.com/home/fish2000/found/'
		imgurls = []
		htmlobject = urllib2.urlopen(url)
		html = htmlobject.read()
		
		for thing in (BeautifulSoup(html).findAll('img', id=re.compile('asset.*'))):
			fff = str(thing['src'])
			imgurls.append(str(fff))
			NSLog(u"HEY GUYS CHECK OUT MY IMAGE URL: "+fff)



	@objc.IBAction
	def parseFFFFoundPageForUserName_(self, sender):
		NSLog(u"FFFFoundDynamicAnalyzer.parseFFFFoundPageForUserName_(): "+sender)
		
		## put the name back into the dropdown
		self._data.append(NSString_stringWithString_(sender))
		
	
	
	@objc.IBAction
	def	getAntsInThePants_(self, sender):
		NSLog(u"I GOT ANTS, IN THE PANTS: ")
	
	
	
	
	###
	### 'private' 'instance methods'
	###
	@objc.IBAction
	def soupForUsername_(self, sender):
		NSLog(u"FFFFoundDynamicAnalyzer.soupForUsername_()")
