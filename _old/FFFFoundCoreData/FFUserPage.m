//
//  FFUserPage.m
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import "FFImage.h"
#import "FFUser.h"
#import "FFUserPage.h"
#import "FFHomePage.h"


@dynamic timeRetrieved;
@dynamic pageType;
@dynamic offset;
@dynamic cachedSourceHandle;
@dynamic pageURL;
@dynamic uiRevisionNumber;
@dynamic images;
@dynamic followers;
@dynamic following;
@dynamic user;

/*
 *
 * You do not need any of these.  
 * These are templates for writing custom functions that override the default CoreData functionality.
 * You should delete all the methods that you do not customize.
 * Optimized versions will be provided dynamically by the framework.
 *
 *
 */


// coalesce these into one @interface FFUserPage (CoreDataGeneratedPrimitiveAccessors) section
@interface FFUserPage (CoreDataGeneratedPrimitiveAccessors)

- (NSDate *)primitiveTimeRetrieved;
- (void)setPrimitiveTimeRetrieved:(NSDate *)value;

- (NSNumber *)primitivePageType;
- (void)setPrimitivePageType:(NSNumber *)value;

- (NSNumber *)primitiveOffset;
- (void)setPrimitiveOffset:(NSNumber *)value;

- (NSString *)primitiveCachedSourceHandle;
- (void)setPrimitiveCachedSourceHandle:(NSString *)value;

- (NSString *)primitivePageURL;
- (void)setPrimitivePageURL:(NSString *)value;

- (NSString *)primitiveUiRevisionNumber;
- (void)setPrimitiveUiRevisionNumber:(NSString *)value;

- (FFUser)primitiveUser;
- (void)setPrimitiveUser:(FFUser)value;

- (NSMutableSet*)primitiveImages;
- (void)setPrimitiveImages:(NSMutableSet*)value;

- (NSMutableSet*)primitiveFollowers;
- (void)setPrimitiveFollowers:(NSMutableSet*)value;

- (NSMutableSet*)primitiveFollowing;
- (void)setPrimitiveFollowing:(NSMutableSet*)value;

@end

- (NSDate *)timeRetrieved 
{
    NSDate * tmpValue;
    
    [self willAccessValueForKey:@"timeRetrieved"];
    tmpValue = [self primitiveTimeRetrieved];
    [self didAccessValueForKey:@"timeRetrieved"];
    
    return tmpValue;
}

- (void)setTimeRetrieved:(NSDate *)value 
{
    [self willChangeValueForKey:@"timeRetrieved"];
    [self setPrimitiveTimeRetrieved:value];
    [self didChangeValueForKey:@"timeRetrieved"];
}

- (BOOL)validateTimeRetrieved:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSNumber *)pageType 
{
    NSNumber * tmpValue;
    
    [self willAccessValueForKey:@"pageType"];
    tmpValue = [self primitivePageType];
    [self didAccessValueForKey:@"pageType"];
    
    return tmpValue;
}

- (void)setPageType:(NSNumber *)value 
{
    [self willChangeValueForKey:@"pageType"];
    [self setPrimitivePageType:value];
    [self didChangeValueForKey:@"pageType"];
}

- (BOOL)validatePageType:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSNumber *)offset 
{
    NSNumber * tmpValue;
    
    [self willAccessValueForKey:@"offset"];
    tmpValue = [self primitiveOffset];
    [self didAccessValueForKey:@"offset"];
    
    return tmpValue;
}

- (void)setOffset:(NSNumber *)value 
{
    [self willChangeValueForKey:@"offset"];
    [self setPrimitiveOffset:value];
    [self didChangeValueForKey:@"offset"];
}

- (BOOL)validateOffset:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)cachedSourceHandle 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"cachedSourceHandle"];
    tmpValue = [self primitiveCachedSourceHandle];
    [self didAccessValueForKey:@"cachedSourceHandle"];
    
    return tmpValue;
}

- (void)setCachedSourceHandle:(NSString *)value 
{
    [self willChangeValueForKey:@"cachedSourceHandle"];
    [self setPrimitiveCachedSourceHandle:value];
    [self didChangeValueForKey:@"cachedSourceHandle"];
}

- (BOOL)validateCachedSourceHandle:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)pageURL 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"pageURL"];
    tmpValue = [self primitivePageURL];
    [self didAccessValueForKey:@"pageURL"];
    
    return tmpValue;
}

- (void)setPageURL:(NSString *)value 
{
    [self willChangeValueForKey:@"pageURL"];
    [self setPrimitivePageURL:value];
    [self didChangeValueForKey:@"pageURL"];
}

- (BOOL)validatePageURL:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)uiRevisionNumber 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"uiRevisionNumber"];
    tmpValue = [self primitiveUiRevisionNumber];
    [self didAccessValueForKey:@"uiRevisionNumber"];
    
    return tmpValue;
}

- (void)setUiRevisionNumber:(NSString *)value 
{
    [self willChangeValueForKey:@"uiRevisionNumber"];
    [self setPrimitiveUiRevisionNumber:value];
    [self didChangeValueForKey:@"uiRevisionNumber"];
}

- (BOOL)validateUiRevisionNumber:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}


- (void)addImagesObject:(FFImage *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveImages] addObject:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeImagesObject:(FFImage *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveImages] removeObject:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addImages:(NSSet *)value 
{    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveImages] unionSet:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeImages:(NSSet *)value 
{
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveImages] minusSet:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addFollowersObject:(FFUser *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveFollowers] addObject:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeFollowersObject:(FFUser *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveFollowers] removeObject:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addFollowers:(NSSet *)value 
{    
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveFollowers] unionSet:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeFollowers:(NSSet *)value 
{
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveFollowers] minusSet:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addFollowingObject:(FFUser *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveFollowing] addObject:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeFollowingObject:(FFUser *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveFollowing] removeObject:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addFollowing:(NSSet *)value 
{    
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveFollowing] unionSet:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeFollowing:(NSSet *)value 
{
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveFollowing] minusSet:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (FFUser *)user 
{
    id tmpObject;
    
    [self willAccessValueForKey:@"user"];
    tmpObject = [self primitiveUser];
    [self didAccessValueForKey:@"user"];
    
    return tmpObject;
}

- (void)setUser:(FFUser *)value 
{
    [self willChangeValueForKey:@"user"];
    [self setPrimitiveUser:value];
    [self didChangeValueForKey:@"user"];
}


- (BOOL)validateUser:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

