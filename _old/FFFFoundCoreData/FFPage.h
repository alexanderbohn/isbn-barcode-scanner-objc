//
//  FFPage.h
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface FFPage : NSObject {
	/// strings for URL of page/feed
	/// (maybe local files, for testing maybe?)
	NSString FFPageURL;
	NSString FFFeedURL;
	
	/// who's page is it, anyway
	NSString userName;
	NSString userPersonalURL;
	NSInteger pageOffset;
	BOOL userPosts; /// true for URLs ending in /post/
	
	/// social data pulled off of the page
	/// key is the user, value is the # of posts for that user
	NSDictionary favoriteUsers;
	NSDictionary followingUsers;
	
	/// image data
	NSArray images;
	
	/// private-ish ffffound metadata
	NSString apparentUIRevision;
}

@end
