//
//  main.m
//  FFFFoundCoreData
//
//  Created by fish on 4/6/09.
//  Copyright Objects In Space And Time, LLC 2009. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[]) {
	
	// I am doing this because the pyobjc website said I should
	NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
	NSString *pluginPath = [[NSBundle mainBundle]
							pathForResource:@"FFFFoundDynamicAnalyzer"
							ofType:@"plugin"];
	
	NSBundle *pluginBundle = [NSBundle bundleWithPath:pluginPath];
	
	[pluginBundle load];
	[pool release];
	
    return NSApplicationMain(argc,  (const char **) argv);
}
