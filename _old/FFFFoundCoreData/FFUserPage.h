//
//  FFUserPage.h
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>

@class FFImage, FFUser, FFUserPage, FFHomePage;

@interface FFUserPage : NSManagedObject {
}

@property (retain) NSDate * timeRetrieved;
@property (retain) NSNumber * pageType;
@property (retain) NSNumber * offset;
@property (retain) NSString * cachedSourceHandle;
@property (retain) NSString * pageURL;
@property (retain) NSString * uiRevisionNumber;
@property (retain) NSSet* images;
@property (retain) NSSet* followers;
@property (retain) NSSet* following;
@property (retain) FFUser * user;


- (void)addImagesObject:(FFImage *)value;
- (void)removeImagesObject:(FFImage *)value;
- (void)addImages:(NSSet *)value;
- (void)removeImages:(NSSet *)value;

- (void)addFollowersObject:(FFUser *)value;
- (void)removeFollowersObject:(FFUser *)value;
- (void)addFollowers:(NSSet *)value;
- (void)removeFollowers:(NSSet *)value;

- (void)addFollowingObject:(FFUser *)value;
- (void)removeFollowingObject:(FFUser *)value;
- (void)addFollowing:(NSSet *)value;
- (void)removeFollowing:(NSSet *)value;

@end
