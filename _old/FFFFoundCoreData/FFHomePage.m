//
//  FFHomePage.m
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import "FFImage.h"
#import "FFUser.h"
#import "FFUserPage.h"
#import "FFHomePage.h"

@implementation FFHomePage

@dynamic timeRetrieved;
@dynamic offset;
@dynamic cachedSourceHandle;
@dynamic pageURL;
@dynamic uiRevisionNumber;
@dynamic images;
@dynamic active;

// coalesce these into one @interface FFHomePage (CoreDataGeneratedPrimitiveAccessors) section
@interface FFHomePage (CoreDataGeneratedPrimitiveAccessors)

- (NSDate *)primitiveTimeRetrieved;
- (void)setPrimitiveTimeRetrieved:(NSDate *)value;

- (NSNumber *)primitiveOffset;
- (void)setPrimitiveOffset:(NSNumber *)value;

- (NSString *)primitiveCachedSourceHandle;
- (void)setPrimitiveCachedSourceHandle:(NSString *)value;

- (NSString *)primitivePageURL;
- (void)setPrimitivePageURL:(NSString *)value;

- (NSString *)primitiveUiRevisionNumber;
- (void)setPrimitiveUiRevisionNumber:(NSString *)value;

- (NSMutableSet*)primitiveImages;
- (void)setPrimitiveImages:(NSMutableSet*)value;

- (NSMutableSet*)primitiveActive;
- (void)setPrimitiveActive:(NSMutableSet*)value;

@end


- (NSDate *)timeRetrieved 
{
    NSDate * tmpValue;
    
    [self willAccessValueForKey:@"timeRetrieved"];
    tmpValue = [self primitiveTimeRetrieved];
    [self didAccessValueForKey:@"timeRetrieved"];
    
    return tmpValue;
}

- (void)setTimeRetrieved:(NSDate *)value 
{
    [self willChangeValueForKey:@"timeRetrieved"];
    [self setPrimitiveTimeRetrieved:value];
    [self didChangeValueForKey:@"timeRetrieved"];
}

- (BOOL)validateTimeRetrieved:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSNumber *)offset 
{
    NSNumber * tmpValue;
    
    [self willAccessValueForKey:@"offset"];
    tmpValue = [self primitiveOffset];
    [self didAccessValueForKey:@"offset"];
    
    return tmpValue;
}

- (void)setOffset:(NSNumber *)value 
{
    [self willChangeValueForKey:@"offset"];
    [self setPrimitiveOffset:value];
    [self didChangeValueForKey:@"offset"];
}

- (BOOL)validateOffset:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)cachedSourceHandle 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"cachedSourceHandle"];
    tmpValue = [self primitiveCachedSourceHandle];
    [self didAccessValueForKey:@"cachedSourceHandle"];
    
    return tmpValue;
}

- (void)setCachedSourceHandle:(NSString *)value 
{
    [self willChangeValueForKey:@"cachedSourceHandle"];
    [self setPrimitiveCachedSourceHandle:value];
    [self didChangeValueForKey:@"cachedSourceHandle"];
}

- (BOOL)validateCachedSourceHandle:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)pageURL 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"pageURL"];
    tmpValue = [self primitivePageURL];
    [self didAccessValueForKey:@"pageURL"];
    
    return tmpValue;
}

- (void)setPageURL:(NSString *)value 
{
    [self willChangeValueForKey:@"pageURL"];
    [self setPrimitivePageURL:value];
    [self didChangeValueForKey:@"pageURL"];
}

- (BOOL)validatePageURL:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)uiRevisionNumber 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"uiRevisionNumber"];
    tmpValue = [self primitiveUiRevisionNumber];
    [self didAccessValueForKey:@"uiRevisionNumber"];
    
    return tmpValue;
}

- (void)setUiRevisionNumber:(NSString *)value 
{
    [self willChangeValueForKey:@"uiRevisionNumber"];
    [self setPrimitiveUiRevisionNumber:value];
    [self didChangeValueForKey:@"uiRevisionNumber"];
}

- (BOOL)validateUiRevisionNumber:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}


- (void)addImagesObject:(FFImage *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveImages] addObject:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeImagesObject:(FFImage *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveImages] removeObject:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addImages:(NSSet *)value 
{    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveImages] unionSet:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeImages:(NSSet *)value 
{
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveImages] minusSet:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addActiveObject:(FFUser *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"active" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveActive] addObject:value];
    [self didChangeValueForKey:@"active" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeActiveObject:(FFUser *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"active" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveActive] removeObject:value];
    [self didChangeValueForKey:@"active" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addActive:(NSSet *)value 
{    
    [self willChangeValueForKey:@"active" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveActive] unionSet:value];
    [self didChangeValueForKey:@"active" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeActive:(NSSet *)value 
{
    [self willChangeValueForKey:@"active" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveActive] minusSet:value];
    [self didChangeValueForKey:@"active" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}

@end
