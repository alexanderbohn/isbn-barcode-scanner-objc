//
//  FFUser.m
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import "FFImage.h"
#import "FFUser.h"
#import "FFUserPage.h"
#import "FFHomePage.h"

@implementation FFUser


- (NSNumber *)totalImageCount 
{
    NSNumber * tmpValue;
    
    [self willAccessValueForKey:@"totalImageCount"];
    tmpValue = [self primitiveTotalImageCount];
    [self didAccessValueForKey:@"totalImageCount"];
    
    return tmpValue;
}

- (void)setTotalImageCount:(NSNumber *)value 
{
    [self willChangeValueForKey:@"totalImageCount"];
    [self setPrimitiveTotalImageCount:value];
    [self didChangeValueForKey:@"totalImageCount"];
}

- (BOOL)validateTotalImageCount:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)userName 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"userName"];
    tmpValue = [self primitiveUserName];
    [self didAccessValueForKey:@"userName"];
    
    return tmpValue;
}

- (void)setUserName:(NSString *)value 
{
    [self willChangeValueForKey:@"userName"];
    [self setPrimitiveUserName:value];
    [self didChangeValueForKey:@"userName"];
}

- (BOOL)validateUserName:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)userSiteURL 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"userSiteURL"];
    tmpValue = [self primitiveUserSiteURL];
    [self didAccessValueForKey:@"userSiteURL"];
    
    return tmpValue;
}

- (void)setUserSiteURL:(NSString *)value 
{
    [self willChangeValueForKey:@"userSiteURL"];
    [self setPrimitiveUserSiteURL:value];
    [self didChangeValueForKey:@"userSiteURL"];
}

- (BOOL)validateUserSiteURL:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}


- (void)addImagesObject:(FFImage *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveImages] addObject:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeImagesObject:(FFImage *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveImages] removeObject:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addImages:(NSSet *)value 
{    
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveImages] unionSet:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeImages:(NSSet *)value 
{
    [self willChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveImages] minusSet:value];
    [self didChangeValueForKey:@"images" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addSuggestedImagesObject:(FFImage *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveSuggestedImages] addObject:value];
    [self didChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeSuggestedImagesObject:(FFImage *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveSuggestedImages] removeObject:value];
    [self didChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addSuggestedImages:(NSSet *)value 
{    
    [self willChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveSuggestedImages] unionSet:value];
    [self didChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeSuggestedImages:(NSSet *)value 
{
    [self willChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveSuggestedImages] minusSet:value];
    [self didChangeValueForKey:@"suggestedImages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addFollowersObject:(FFUser *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveFollowers] addObject:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeFollowersObject:(FFUser *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveFollowers] removeObject:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addFollowers:(NSSet *)value 
{    
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveFollowers] unionSet:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeFollowers:(NSSet *)value 
{
    [self willChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveFollowers] minusSet:value];
    [self didChangeValueForKey:@"followers" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addFollowingObject:(FFUser *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveFollowing] addObject:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeFollowingObject:(FFUser *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveFollowing] removeObject:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addFollowing:(NSSet *)value 
{    
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveFollowing] unionSet:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeFollowing:(NSSet *)value 
{
    [self willChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveFollowing] minusSet:value];
    [self didChangeValueForKey:@"following" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addUserPagesObject:(FFUserPage *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"userPages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveUserPages] addObject:value];
    [self didChangeValueForKey:@"userPages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeUserPagesObject:(FFUserPage *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"userPages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveUserPages] removeObject:value];
    [self didChangeValueForKey:@"userPages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addUserPages:(NSSet *)value 
{    
    [self willChangeValueForKey:@"userPages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveUserPages] unionSet:value];
    [self didChangeValueForKey:@"userPages" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeUserPages:(NSSet *)value 
{
    [self willChangeValueForKey:@"userPages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveUserPages] minusSet:value];
    [self didChangeValueForKey:@"userPages" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


@end