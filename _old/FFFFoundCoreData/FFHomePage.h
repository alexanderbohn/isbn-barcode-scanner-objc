//
//  FFHomePage.h
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>

@class FFImage, FFUser, FFUserPage, FFHomePage;

@interface FFHomePage : NSManagedObject {
}

@property (retain) NSDate * timeRetrieved;
@property (retain) NSNumber * offset;
@property (retain) NSString * cachedSourceHandle;
@property (retain) NSString * pageURL;
@property (retain) NSString * uiRevisionNumber;
@property (retain) NSSet* images;
@property (retain) NSSet* active;


- (void)addImagesObject:(FFImage *)value;
- (void)removeImagesObject:(FFImage *)value;
- (void)addImages:(NSSet *)value;
- (void)removeImages:(NSSet *)value;

- (void)addActiveObject:(FFUser *)value;
- (void)removeActiveObject:(FFUser *)value;
- (void)addActive:(NSSet *)value;
- (void)removeActive:(NSSet *)value;

@end


