//
//  FFFFoundCoreData_AppDelegate.h
//  FFFFoundCoreData
//
//  Created by fish on 4/6/09.
//  Copyright Objects In Space And Time, LLC 2009 . All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>

@interface FFFFoundCoreData_AppDelegate : NSObject 
{
    IBOutlet NSWindow *window;
    
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
	
	
	/// need to stick values up in here, for bindings.
	
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObjectModel *)managedObjectModel;
- (NSManagedObjectContext *)managedObjectContext;

- (IBAction)saveAction:sender;

@end
