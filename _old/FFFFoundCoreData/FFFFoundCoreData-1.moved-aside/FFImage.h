//
//  FFImage.h
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface FFImage : NSObject {
	/// ID info for the image
	NSInteger imageID;
	NSString hashString;
	
	
	
	/// private-ish ffffound metadata
	NSString apparentUIRevision;
}

@end
