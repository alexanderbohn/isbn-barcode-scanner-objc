#!/opt/local/bin/python2.6
#
# (shebang was /usr/bin/env python2.3)
#
#
#  setup.py
#  FFFFoundCoreData
#
#  Created by fish on 4/7/09.
#  Copyright (c) 2009 Objects In Space And Time, LLC. All rights reserved.
#  straight from http://pyobjc.sourceforge.net/documentation/pyobjc-core/tutorial_embed/index.html

#from distutils.core import setup
import sys, py2app, objc, os
from BeautifulSoup import BeautifulSoup
from setuptools import setup

#	plugin = ["FFFFoundDynamicAnalyzer.py"],


setup(
	plugin = ["FFFFoundDynamicAnalyzer.py"],
	setup_requires = [
		"py2app",
		"pyobjc",
		"BeautifulSoup",
	],
	options=dict(
		py2app=dict(
			plist=dict(
				CFBundleSignature="FFFf",
				CFBundleIdentifier="com.objectsinspaceandtime.FFFFoundDynamicAnalyzer",
				NSPrincipalClass="FFFFoundDynamicAnalyzer",
			)
		)
	)
)
