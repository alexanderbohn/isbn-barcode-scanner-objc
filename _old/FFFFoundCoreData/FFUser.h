//
//  FFUser.h
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>

@class FFImage, FFUser, FFUserPage, FFHomePage;

@interface FFUser: NSManagedObject {
	@dynamic totalImageCount;
	@dynamic userName;
	@dynamic userSiteURL;
	@dynamic images;
	@dynamic suggestedImages;
	@dynamic followers;
	@dynamic following;
	@dynamic userPages;
}

@property (retain) NSNumber * totalImageCount;
@property (retain) NSString * userName;
@property (retain) NSString * userSiteURL;
@property (retain) NSSet* images;
@property (retain) NSSet* suggestedImages;
@property (retain) NSSet* followers;
@property (retain) NSSet* following;
@property (retain) NSSet* userPages;


- (void)addImagesObject:(FFImage *)value;
- (void)removeImagesObject:(FFImage *)value;
- (void)addImages:(NSSet *)value;
- (void)removeImages:(NSSet *)value;

- (void)addSuggestedImagesObject:(FFImage *)value;
- (void)removeSuggestedImagesObject:(FFImage *)value;
- (void)addSuggestedImages:(NSSet *)value;
- (void)removeSuggestedImages:(NSSet *)value;

- (void)addFollowersObject:(FFUser *)value;
- (void)removeFollowersObject:(FFUser *)value;
- (void)addFollowers:(NSSet *)value;
- (void)removeFollowers:(NSSet *)value;

- (void)addFollowingObject:(FFUser *)value;
- (void)removeFollowingObject:(FFUser *)value;
- (void)addFollowing:(NSSet *)value;
- (void)removeFollowing:(NSSet *)value;

- (void)addUserPagesObject:(FFUserPage *)value;
- (void)removeUserPagesObject:(FFUserPage *)value;
- (void)addUserPages:(NSSet *)value;
- (void)removeUserPages:(NSSet *)value;

- (NSNumber *)primitiveTotalImageCount;
- (void)setPrimitiveTotalImageCount:(NSNumber *)value;

- (NSString *)primitiveUserName;
- (void)setPrimitiveUserName:(NSString *)value;

- (NSString *)primitiveUserSiteURL;
- (void)setPrimitiveUserSiteURL:(NSString *)value;

- (NSMutableSet*)primitiveImages;
- (void)setPrimitiveImages:(NSMutableSet*)value;

- (NSMutableSet*)primitiveSuggestedImages;
- (void)setPrimitiveSuggestedImages:(NSMutableSet*)value;

- (NSMutableSet*)primitiveFollowers;
- (void)setPrimitiveFollowers:(NSMutableSet*)value;

- (NSMutableSet*)primitiveFollowing;
- (void)setPrimitiveFollowing:(NSMutableSet*)value;

- (NSMutableSet*)primitiveUserPages;
- (void)setPrimitiveUserPages:(NSMutableSet*)value;

@end
