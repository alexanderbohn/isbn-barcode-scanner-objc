//
//  FFImage.h
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreData/CoreData.h>

@class FFImage, FFUser, FFUserPage, FFHomePage;

@interface FFImage : NSManagedObject {
}

@property (retain) NSString * originalURL;
@property (retain) NSNumber * imageID;
@property (retain) NSString * cachedImageHandle;
@property (retain) NSString * hash;
@property (retain) NSString * quotedFromPageName;
@property (retain) NSString * quotedFromURL;
@property (retain) NSSet* similarTo;
@property (retain) FFUser * imageAddedBy;
@property (retain) NSSet* imageLovedBy;
@property (retain) NSSet* fromUserPage;


- (void)addSimilarToObject:(FFImage *)value;
- (void)removeSimilarToObject:(FFImage *)value;
- (void)addSimilarTo:(NSSet *)value;
- (void)removeSimilarTo:(NSSet *)value;

- (void)addImageLovedByObject:(FFUser *)value;
- (void)removeImageLovedByObject:(FFUser *)value;
- (void)addImageLovedBy:(NSSet *)value;
- (void)removeImageLovedBy:(NSSet *)value;

- (void)addFromUserPageObject:(FFUserPage *)value;
- (void)removeFromUserPageObject:(FFUserPage *)value;
- (void)addFromUserPage:(NSSet *)value;
- (void)removeFromUserPage:(NSSet *)value;

@end
