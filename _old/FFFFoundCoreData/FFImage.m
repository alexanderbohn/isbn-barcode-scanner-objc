//
//  FFImage.m
//  FFFFoundCoreData
//
//  Created by fish on 4/7/09.
//  Copyright 2009 Objects In Space And Time, LLC. All rights reserved.
//

#import "FFImage.h"
#import "FFUser.h"
#import "FFUserPage.h"
#import "FFHomePage.h"

@dynamic imageID;
@dynamic cachedImageHandle;
@dynamic hash;
@dynamic originalURL;
@dynamic quotedFromPageName;
@dynamic quotedFromURL;
@dynamic similarTo;
@dynamic imageAddedBy;
@dynamic imageLovedBy;
@dynamic fromUserPage;

/*
 *
 * You do not need any of these.  
 * These are templates for writing custom functions that override the default CoreData functionality.
 * You should delete all the methods that you do not customize.
 * Optimized versions will be provided dynamically by the framework.
 *
 *
 */


// coalesce these into one @interface FFImage (CoreDataGeneratedPrimitiveAccessors) section
@interface FFImage (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber *)primitiveImageID;
- (void)setPrimitiveImageID:(NSNumber *)value;

- (NSString *)primitiveCachedImageHandle;
- (void)setPrimitiveCachedImageHandle:(NSString *)value;

- (NSString *)primitiveHash;
- (void)setPrimitiveHash:(NSString *)value;

- (NSString *)primitiveOriginalURL;
- (void)setPrimitiveOriginalURL:(NSString *)value;

- (NSString *)primitiveQuotedFromPageName;
- (void)setPrimitiveQuotedFromPageName:(NSString *)value;

- (NSString *)primitiveQuotedFromURL;
- (void)setPrimitiveQuotedFromURL:(NSString *)value;

- (FFUser)primitiveImageAddedBy;
- (void)setPrimitiveImageAddedBy:(FFUser)value;

- (NSMutableSet*)primitiveSimilarTo;
- (void)setPrimitiveSimilarTo:(NSMutableSet*)value;

- (NSMutableSet*)primitiveImageLovedBy;
- (void)setPrimitiveImageLovedBy:(NSMutableSet*)value;

- (NSMutableSet*)primitiveFromUserPage;
- (void)setPrimitiveFromUserPage:(NSMutableSet*)value;

@end

- (NSNumber *)imageID 
{
    NSNumber * tmpValue;
    
    [self willAccessValueForKey:@"imageID"];
    tmpValue = [self primitiveImageID];
    [self didAccessValueForKey:@"imageID"];
    
    return tmpValue;
}

- (void)setImageID:(NSNumber *)value 
{
    [self willChangeValueForKey:@"imageID"];
    [self setPrimitiveImageID:value];
    [self didChangeValueForKey:@"imageID"];
}

- (BOOL)validateImageID:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)cachedImageHandle 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"cachedImageHandle"];
    tmpValue = [self primitiveCachedImageHandle];
    [self didAccessValueForKey:@"cachedImageHandle"];
    
    return tmpValue;
}

- (void)setCachedImageHandle:(NSString *)value 
{
    [self willChangeValueForKey:@"cachedImageHandle"];
    [self setPrimitiveCachedImageHandle:value];
    [self didChangeValueForKey:@"cachedImageHandle"];
}

- (BOOL)validateCachedImageHandle:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)hash 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"hash"];
    tmpValue = [self primitiveHash];
    [self didAccessValueForKey:@"hash"];
    
    return tmpValue;
}

- (void)setHash:(NSString *)value 
{
    [self willChangeValueForKey:@"hash"];
    [self setPrimitiveHash:value];
    [self didChangeValueForKey:@"hash"];
}

- (BOOL)validateHash:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)originalURL 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"originalURL"];
    tmpValue = [self primitiveOriginalURL];
    [self didAccessValueForKey:@"originalURL"];
    
    return tmpValue;
}

- (void)setOriginalURL:(NSString *)value 
{
    [self willChangeValueForKey:@"originalURL"];
    [self setPrimitiveOriginalURL:value];
    [self didChangeValueForKey:@"originalURL"];
}

- (BOOL)validateOriginalURL:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)quotedFromPageName 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"quotedFromPageName"];
    tmpValue = [self primitiveQuotedFromPageName];
    [self didAccessValueForKey:@"quotedFromPageName"];
    
    return tmpValue;
}

- (void)setQuotedFromPageName:(NSString *)value 
{
    [self willChangeValueForKey:@"quotedFromPageName"];
    [self setPrimitiveQuotedFromPageName:value];
    [self didChangeValueForKey:@"quotedFromPageName"];
}

- (BOOL)validateQuotedFromPageName:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}

- (NSString *)quotedFromURL 
{
    NSString * tmpValue;
    
    [self willAccessValueForKey:@"quotedFromURL"];
    tmpValue = [self primitiveQuotedFromURL];
    [self didAccessValueForKey:@"quotedFromURL"];
    
    return tmpValue;
}

- (void)setQuotedFromURL:(NSString *)value 
{
    [self willChangeValueForKey:@"quotedFromURL"];
    [self setPrimitiveQuotedFromURL:value];
    [self didChangeValueForKey:@"quotedFromURL"];
}

- (BOOL)validateQuotedFromURL:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}


- (void)addSimilarToObject:(FFImage *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveSimilarTo] addObject:value];
    [self didChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeSimilarToObject:(FFImage *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveSimilarTo] removeObject:value];
    [self didChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addSimilarTo:(NSSet *)value 
{    
    [self willChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveSimilarTo] unionSet:value];
    [self didChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeSimilarTo:(NSSet *)value 
{
    [self willChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveSimilarTo] minusSet:value];
    [self didChangeValueForKey:@"similarTo" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (FFUser *)imageAddedBy 
{
    id tmpObject;
    
    [self willAccessValueForKey:@"imageAddedBy"];
    tmpObject = [self primitiveImageAddedBy];
    [self didAccessValueForKey:@"imageAddedBy"];
    
    return tmpObject;
}

- (void)setImageAddedBy:(FFUser *)value 
{
    [self willChangeValueForKey:@"imageAddedBy"];
    [self setPrimitiveImageAddedBy:value];
    [self didChangeValueForKey:@"imageAddedBy"];
}


- (BOOL)validateImageAddedBy:(id *)valueRef error:(NSError **)outError 
{
    // Insert custom validation logic here.
    return YES;
}


- (void)addImageLovedByObject:(FFUser *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveImageLovedBy] addObject:value];
    [self didChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeImageLovedByObject:(FFUser *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveImageLovedBy] removeObject:value];
    [self didChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addImageLovedBy:(NSSet *)value 
{    
    [self willChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveImageLovedBy] unionSet:value];
    [self didChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeImageLovedBy:(NSSet *)value 
{
    [self willChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveImageLovedBy] minusSet:value];
    [self didChangeValueForKey:@"imageLovedBy" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


- (void)addFromUserPageObject:(FFUserPage *)value 
{    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveFromUserPage] addObject:value];
    [self didChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)removeFromUserPageObject:(FFUserPage *)value 
{
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    
    [self willChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveFromUserPage] removeObject:value];
    [self didChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    
    [changedObjects release];
}

- (void)addFromUserPage:(NSSet *)value 
{    
    [self willChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveFromUserPage] unionSet:value];
    [self didChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeFromUserPage:(NSSet *)value 
{
    [self willChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveFromUserPage] minusSet:value];
    [self didChangeValueForKey:@"fromUserPage" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


