//
//  JSONHTTPOperation.m
//  BarcodeScanner
//
//  Created by FI$H 2000 on 3/20/10.
//  Copyright 2010 Objects In Space And Time, LLC. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <JSON/JSON.h>
#import "JSONHTTPOperation.h"
#import "MyController.h"

@implementation JSONHTTPOperation;

@synthesize ISBN;
@synthesize endpt;


- (id)initWithURL:(NSString*)url
		  forISBN:(NSString*)anISBN {
	//NSLog(@"JSONHTTPOperation initWithURL:forISBN");
	if (![super init]) return nil;
	[self setISBN:anISBN];
	[self setEndpt:url];
	
	return self;
}


- (void)dealloc {
	NSLog(@"JSONHTTPOperation dealloc");
	[endpt release], endpt = nil;
	[ISBN release], ISBN = nil;
	[super dealloc];
}


- (void)main {
	//NSLog(@"JSONHTTPOperation main");
	SBJSON *json = [SBJSON new];
	NSURL *url = [NSURL URLWithString:[[self endpt] stringByAppendingString:[self ISBN]]];
	
	//NSLog(@"JSON URL: %@", url);
	NSError *error = nil;
	NSString *jsonstring = [[[NSString alloc] initWithContentsOfURL:url] autorelease];
	NSDictionary *jsonout = [[[NSDictionary alloc] initWithDictionary:[json objectWithString:jsonstring error:&error]] autorelease];
	
	if (!jsonout) {
		[[MyController shared] performSelectorOnMainThread:@selector(outputError:)
											   withObject:error
											waitUntilDone:YES];
		[jsonout release];
		return;
	}
	
	[[MyController shared] performSelectorOnMainThread:@selector(outputResults:)
										   withObject:[jsonout copy]
										waitUntilDone:YES];
	
	[jsonout release];
	[json release];
}


@end
