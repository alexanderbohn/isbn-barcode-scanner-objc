//
//  main.m
//  BarcodeScanner
//
//  Created by Conor Dearden on 3/11/06.
//  Copyright Chiguire LLC 2006. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
	/*
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    int retVal = NSApplicationMain(argc,  (const char **) argv);
    [pool release];
    return retVal;
	*/
	return NSApplicationMain(argc,  (const char **) argv);
}
