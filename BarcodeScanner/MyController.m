/*
 MyController.m is part of BarcodeScanner.
 */

#import "MyController.h"
#import "MyBarcodeScanner.h"


@implementation MyController

static MyController *shared;
static NSString *endpt;

- (IBAction)openiSightOneScan:(id)sender {
	
	//Get isight object and set values
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	MyBarcodeScanner *iSight = [MyBarcodeScanner sharedInstance];
	[iSight setStaysOpen:NO];
	[iSight setDelegate:self];
	
	//If option was held down, flip the mirrored preference value
	if ([sender isKindOfClass:[NSButton class]]) {
		if ([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask) {
			int mirrorValue = ([[prefs objectForKey:@"Mirror iSight"] intValue] +1) %2;
			[prefs setObject:[NSNumber numberWithInt:mirrorValue] forKey:@"Mirror iSight"];
		}
	}
	
	//Set if mirrored
	if ([[prefs objectForKey:@"Mirror iSight"] intValue])
		[iSight setMirrored:NO];
	else
		[iSight setMirrored:YES];
	
	//Begin scan
	[iSight scanForBarcodeWindow:nil];
}


/*---  Same as above but by staysOpen is set to YES  
	Scanning continously won't scan the same barcode twice
	as it remembers the last scan.
--*/

- (IBAction)openiSightContinousScanning:(id)sender {
	NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
	MyBarcodeScanner *iSight = [MyBarcodeScanner sharedInstance];
	[iSight setStaysOpen:YES];
	[iSight setDelegate:self];
	
	// If option was held down, flip the mirrored preference value
	/// SIKE!!! yo maybe we'll flip it if we DONT HOLD DOWN OPTION
	if ([sender isKindOfClass:[NSButton class]]) {
		if ([[NSApp currentEvent] modifierFlags] & NSAlternateKeyMask) {
			int mirrorValue = ([[prefs objectForKey:@"Mirror iSight"] intValue] +1) %2;
			[prefs setObject:[NSNumber numberWithInt:mirrorValue] forKey:@"Mirror iSight"];
		}
	}
	
	if ([[prefs objectForKey:@"Mirror iSight"] intValue])
		[iSight setMirrored:NO];
	else
		[iSight setMirrored:YES];
	
	[iSight scanForBarcodeWindow:nil];
}

#pragma mark -
#pragma mark delegate

- (id)init {
	NSLog(@"MyController init");
	if (shared) {
		[self autorelease];
		return shared;
	}
	if (![super init]) return nil;
	
	//endpt = @"http://otus-asio.local/api/scan/isbn/";
	endpt = @"http://forsale.objectsinspaceandtime.com/api/scan/isbn/";
	queue = [[NSOperationQueue alloc] init];
	shared = self;
	return self;
}

+ (id)shared {
	if (!shared) {
		[[MyController alloc] init];
	}
	return shared;
}

- (void)iSightWillClose {
	[[resultsTextView window] makeKeyAndOrderFront:self];
}

- (void)gotBarcode:(NSString *)barcode {
	[resultsTextView replaceCharactersInRange:NSMakeRange([[resultsTextView string] length],0) withString:barcode];
	[resultsTextView replaceCharactersInRange:NSMakeRange([[resultsTextView string] length],0) withString:@"\n"];
	
	[queue cancelAllOperations];
	JSONHTTPOperation *op = [[JSONHTTPOperation alloc] initWithURL:[[[NSString alloc] initWithString:endpt] autorelease]
														   forISBN:[barcode copy]];
	[queue addOperation:op];	

}


- (void)outputResults:(NSDictionary*)whatwegot {
	NSLog(@"MyController outputResults: %@", whatwegot);
	
	//(NSString *)
	//NSLog(@"Result status type: %@", [[whatwegot objectForKey:@"status"] class]);
	
	if (whatwegot) {
		
		if ([[whatwegot objectForKey:@"status"] isEqualToString:@"ok"]) {
			/*
			NSDictionary *fsdataset = [[[NSDictionary alloc]
										initWithDictionary:[[whatwegot
															 objectForKey:@"data"] objectForKey:@"dataset"]]
									   autorelease];
			*/
			NSDictionary *fsdata = [[[NSDictionary alloc]
									 initWithDictionary:[whatwegot
														 objectForKey:@"data"]]
									autorelease];
			
			NSString *outout = [NSString stringWithFormat:@"%@ (%@) [OK]",
								[fsdata objectForKey:@"title"],
								[fsdata objectForKey:@"ISBN"]];
			
			[self output:outout];
		} else {
			NSDictionary *fsdata = [[[NSDictionary alloc]
									 initWithDictionary:[whatwegot
														 objectForKey:@"data"]]
									autorelease];
			NSString *outout = [NSString stringWithFormat:@"ERROR -- %@ [FAIL]",
								[fsdata objectForKey:@"msg"]];
			
			[self output:outout];
		}
		
	}
	
}

- (void)outputError:(NSError*)whatwegot {
	NSLog(@"MyController outputError");
	
	NSString *whatstr = [[[NSString alloc] initWithFormat:@"ERROR: %@", whatwegot] autorelease]; 
}

- (void)output:(NSString*)outputThis {
	NSLog(@"OUTPUT: %@", outputThis);
	//[outputJSONResults setStringValue: outputThis];
	[resultsTextView replaceCharactersInRange:NSMakeRange([[resultsTextView string] length],0) withString:outputThis];
	[resultsTextView replaceCharactersInRange:NSMakeRange([[resultsTextView string] length],0) withString:@"\n"];

}



- (void)dealloc {
	
	NSLog(@"MyController dealloc");
	[queue cancelAllOperations];
	[queue release], queue = nil;
	[super dealloc];
	
}


- (void)applicationDidFinishLaunching:(NSNotification*)notification
{	
#ifdef DEBUG
	[self openiSightOneScan:nil];
#endif DEBUG
}

@end
