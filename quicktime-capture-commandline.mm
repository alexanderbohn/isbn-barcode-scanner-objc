// quicktime command-line capture sample code
// from http://lists.apple.com/archives/quicktime-api/2008/Jul/msg00113.html

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <QTKit/QTKit.h>

int cnt = 0;

@interface QTCViewDelegate:NSObject {}

- (CIImage *)view:(QTCaptureView *)view willDisplayImage :(CIImage *)image;

@end

@implementation QTCViewDelegate

- (CIImage *)view:(QTCaptureView *)view willDisplayImage :(CIImage *)image {
if (cnt==0) {
NSBitmapImageRep *rep = [[NSBitmapImageRep alloc] initWithCIImage:image];
[[rep TIFFRepresentation] writeToFile:@"foo.tiff" atomically:NO];
}
cnt++;
return image;
}

@end

@interface myThread:NSThread {}

- (void)main;

@end

@implementation myThread:NSThread

- (void)main {
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
  while (cnt<1) {
    sleep(1);
    NSLog(@"Captured %d frames", cnt);
  }
  [NSApp terminate:self];
  [pool release];
}

@end


int main( int argc, const char* argv[])
{

  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  [NSApplication sharedApplication];

NSArray *a = [QTCaptureDevice inputDevicesWithMediaType:QTMediaTypeVideo];
QTCaptureDevice *cam = [a objectAtIndex:0];
NSError *err;
if ([cam open:&err] != YES) {
NSLog(@"Error opening camera: %@", err);
}
QTCaptureDeviceInput *in = [[QTCaptureDeviceInput alloc] initWithDevice:cam];
QTCaptureSession *session = [[QTCaptureSession alloc] init];
if ([session addInput:in error:&err] != YES) {
NSLog(@"Error adding input to capture session: %@", err);
}
QTCaptureView *v = [[QTCaptureView alloc] init];
[v setCaptureSession:session];
[v setDelegate:[QTCViewDelegate alloc]];
[session startRunning];

  // Not really needed, but it's kinda cool
  NSWindow *win = [[NSWindow alloc] initWithContentRect:NSMakeRect
                                    (50, 50, 600, 400)
                                    styleMask:15
                                    backing:NSBackingStoreBuffered
                                    defer:NO];
  [win setContentView:v];
  [win makeKeyAndOrderFront:win];

  NSLog(@"Capturing from: %@ %d", cam, [session isRunning]);
  [[[myThread alloc] init] start];
  [NSApp run];
  NSLog(@"Captured %d frames", cnt);

  [pool release];
  return 0;
}